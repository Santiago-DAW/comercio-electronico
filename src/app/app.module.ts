import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
// Header:
import { HeaderComponent } from './components/header/header.component';
import { HeaderModule } from './components/header/header.module';
// Body:
import { BodyComponent } from './components/body/body.component';
import { BodyModule } from './components/body/body.module';
// Footer:
import { FooterComponent } from './components/footer/footer.component';
import { FooterModule } from './components/footer/footer.module';
// Modulos:
import { CustomFormsModule } from './common/modules/custom-forms/custom-forms.module';

@NgModule({
  declarations: [
    AppComponent,
      HeaderComponent,
      BodyComponent,
      FooterComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HeaderModule,
    BodyModule,
    FooterModule,
    CustomFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
