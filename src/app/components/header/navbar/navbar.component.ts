import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  elementos_navbar: any[];

  constructor() {
    this.elementos_navbar = [
      { name: 'Inicio', path: '' },
      { name: 'Catálogo de Plantas', path: 'plantas' },
      { name: 'Macetas y Jardineras', path: 'macetas' },
      { name: 'Ramos de Flores', path: 'flores' },
      { name: 'Nuestra Historia', path: 'historia' },
      { name: 'Contacto', path: 'contacto' }
    ];
  }
}
