import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  titulo: string;
  eslogan: string;

  constructor() {
    this.titulo = 'La Selva Verde';
    this.eslogan = 'Plantas que llenan tu vida de color';
  }
}
