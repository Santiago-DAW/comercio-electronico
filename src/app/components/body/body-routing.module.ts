import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { CategorySelectorComponent } from './pages/category-selector/category-selector.component';
import { HistoryComponent } from './pages/history/history.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ErrorComponent } from './pages/error/error.component';

const bodyRoutes: Routes = [
  {path:'', component:HomeComponent },
  {path:'plantas', component:CategorySelectorComponent },
  {path:'macetas', component:CategorySelectorComponent },
  {path:'flores', component:CategorySelectorComponent },
  {path:'historia', component:HistoryComponent },
  {path:'contacto', component:ContactComponent },
  {path:'**', component:ErrorComponent },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(bodyRoutes)
  ],
  exports: [RouterModule]
})
export class BodyRoutingModule { }
