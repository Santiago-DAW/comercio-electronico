import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyRoutingModule as BodyRouterModule } from './body-routing.module';

import { HomeComponent } from './pages/home/home.component';
import { CategorySelectorComponent } from './pages/category-selector/category-selector.component';
import { ProductGalleryComponent } from './pages/product-gallery/product-gallery.component';
import { HistoryComponent } from './pages/history/history.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ErrorComponent } from './pages/error/error.component';

@NgModule({
  declarations: [
    HomeComponent,
    CategorySelectorComponent,
      ProductGalleryComponent,
    HistoryComponent,
    ContactComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    BodyRouterModule
  ]
})
export class BodyModule { }
