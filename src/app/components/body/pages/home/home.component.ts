import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  plantsCatalog: string[];

  constructor() {
    this.plantsCatalog = [
      'Plantas de interior',
      'Plantas de exterior',
      'Plantas para acuarios',
      'Plantas de temporada'
    ];
  }
}
