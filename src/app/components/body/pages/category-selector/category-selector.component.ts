import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-category-selector',
  templateUrl: './category-selector.component.html',
  styleUrls: ['./category-selector.component.css']
})
export class CategorySelectorComponent {
  title: string;
  categories: string[];
  currentCategory: string;

  constructor(private router: Router) {
    this.title = '';
    this.categories = ['plants', 'pots', 'flowers'];
    this.currentCategory = this.getCategory(router.url);
  }
  
  ngOnInit() {
    this.loadCategoryData();
  }
  
  getCategory(url: string): string {
    if (url) return url.split('/')[1];
    return '';  
  }

  loadCategoryData(): void {
    this.title = this.currentCategory;
  }
}
