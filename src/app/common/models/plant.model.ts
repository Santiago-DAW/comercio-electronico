export interface Plant {
  id: number,
  ref: string,
  title: string,
  images: string[],
  categories: string[],
  price: number,
  info: {
    height: number,
    size: string,
    description: string
  }
}
