import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

const formField: any = {
  name: {type: 'text', text: 'Nombre', placeholder: ''},
  surname: {type: 'text', text: 'Apellidos', placeholder: ''},
  email: {type: 'email', text: 'Correo', placeholder: ''},
  phone: {type: 'tel', text: 'Teléfono', placeholder: ''},
  password: {type: 'password', text: 'Contraseña', placeholder: ''},
  confirmPassword: {type: 'password', text: 'Confirmar contraseña', placeholder: ''}
};

@Component({
  selector: 'app-form-sign-up',
  templateUrl: './form-sign-up.component.html',
  styleUrls: ['./form-sign-up.component.css']
})
export class FormSignUpComponent {
  formSignUp: FormGroup;
  inputData: any = this.generateInputData();

  constructor(private formBuilder: FormBuilder) {
    this.formSignUp = this.createFormSignUp();
    this.inputData = this.generateInputData();
  }

  createFormSignUp(): FormGroup {
    const { required, email, minLength, maxLength} = Validators;
    // añadir expresión regular para validar los campos de nombre y apellidos
    const nameValidator: Validators[] = [required, maxLength(60)];
    const surnameValidator: Validators[] = [required, maxLength(100)];
    const emailValidator: Validators[] = [required, email, maxLength(255)];
    // añadir expresión regular para validar telefonos
    const phoneValidator: Validators[] = [minLength(7), maxLength(15)];
    const passwordValidator: Validators[] = [required, minLength(8), maxLength(20)];

    return this.formBuilder.group({
      name: ['', nameValidator],
      surname: ['', surnameValidator],
      email: ['', emailValidator],
      phone: ['', phoneValidator],
      password: ['', passwordValidator],
      confirmPassword: ['', passwordValidator]
    });
  }

  generateInputData() {
    return Object.keys(formField).map(key => {
      return { name: key, id: `formSignUp_${key}`, ...formField[key] };
    });
  }

  onSubmit(): void {
    console.log(this.formSignUp.value);
  }
}
