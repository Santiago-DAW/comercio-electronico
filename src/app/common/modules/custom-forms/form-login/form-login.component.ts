import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const formField: any = {
  name: {type: 'text', name: 'user', text: 'Usuario', placeholder: 'Introduzca usuario'},
  pass: {type: 'password', name: 'password', text: 'Contraseña', placeholder: 'Introduzca contraseña'},
};

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent {
  formLogin: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formLogin = this.createFormLogin();
  }

  createFormLogin(): FormGroup {
    const { required, minLength, maxLength } = Validators;
    const userValidators = [required, minLength(5), maxLength(20)];
    const passwordValidators = [required, maxLength(20)];
    
    return this.formBuilder.group({
      user: ['', userValidators],
      password: ['', passwordValidators],
    });
  }

  onSubmit(): void {
    console.log(this.formLogin.value);
  }
}
