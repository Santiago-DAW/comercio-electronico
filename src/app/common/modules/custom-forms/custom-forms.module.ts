import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FormLoginComponent } from './form-login/form-login.component';
import { FormSignUpComponent } from './form-sign-up/form-sign-up.component';

import { FormComponentsModule } from '../form-components/form-components.module';

@NgModule({
  declarations: [
    FormLoginComponent,
    FormSignUpComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormComponentsModule
  ],
  exports: [
    FormLoginComponent,
    FormSignUpComponent
  ]
})
export class CustomFormsModule { }
