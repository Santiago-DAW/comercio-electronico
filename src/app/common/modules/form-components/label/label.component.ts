import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.css']
})
export class LabelComponent {
  @Input() data: any;

  constructor() {
    this.data = this.generateLabelData(0);
  }

  ngOnInit() {
    this.data = this.generateLabelData(this.data);
  }

  generateLabelData(data: any): object {
    return {
      id: data.id || '',
      text: data.text || '',
      class: data.class || 'form-label'
    };
  }
}
