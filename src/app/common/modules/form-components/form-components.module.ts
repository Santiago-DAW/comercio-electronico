import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { InputComponent } from './input/input.component';
import { LabelComponent } from './label/label.component';
import { LabelInputComponent } from './label-input/label-input.component';

@NgModule({
  declarations: [
    InputComponent,
    LabelComponent,
    LabelInputComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    InputComponent,
    LabelComponent,
    LabelInputComponent
  ]
})
export class FormComponentsModule { }
