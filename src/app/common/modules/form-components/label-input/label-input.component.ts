import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-label-input',
  templateUrl: './label-input.component.html',
  styleUrls: ['./label-input.component.css']
})
export class LabelInputComponent {
  @Input() data: any;
  @Input() control: any;

  constructor() {
    this.data = this.generateData(0);
  }

  ngOnInit() {
    this.data = this.generateData(this.data);
  }

  generateData(data: any): object {
    return {
      type: data.type || '',
      name: data.name || '',
      id: data.id || '',
      text: data.text || '',
      placeholder: data.placeholder || '',
      class: data.class || 'form-control'
    }
  }
}
