import { Component, EventEmitter, Input, Output, SimpleChange } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent {
  @Input() data: any;
  @Input() control: any;
  
  @Output() event: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    this.data = this.generateInputData(0);
    // this.control = new FormControl('');
  }

  ngOnInit() {
    this.data = this.generateInputData(this.data);
  }

  generateInputData(data: any): object {
    return {
      type: data.type || '',
      name: data.name || '',
      id: data.id || '',
      text: data.text || '',
      placeholder: data.placeholder || '',
      class: data.class || 'form-control'
    };
  }
}
