import { Injectable } from '@angular/core';
import { Plant } from '../models/plant.model';

@Injectable({
  providedIn: 'root'
})
export class PlantsDataService {
  dummyPlantsData: Plant[] = [
    {
      id: 1,
      ref: '1',
      title: 'titulo planta 1',
      images: ['imagen 1.1', 'imagen 1.2'],
      categories: ['categoria 1, planta 1', 'categoria 2, planta 1'],
      price: 1.99,
      info: {
        height: 1.5,
        size: 'mediano de planta 1',
        description: 'descripcion planta 1'
      }
    },
    {
      id: 2,
      ref: '2',
      title: 'titulo planta 2',
      images: ['imagen 2.1', 'imagen 2.2'],
      categories: ['categoria 1, planta 2', 'categoria 2, planta 2'],
      price: 1.99,
      info: {
        height: 1.5,
        size: 'mediano de planta 2',
        description: 'descripcion planta 2'
      }
    },
    {
      id: 3,
      ref: '3',
      title: 'titulo planta 3',
      images: ['imagen 3.1', 'imagen 3.2'],
      categories: ['categoria 1, planta 3', 'categoria 2, planta 3'],
      price: 1.99,
      info: {
        height: 1.5,
        size: 'mediano de planta 3',
        description: 'descripcion planta 3'
      }
    },
    {
      id: 4,
      ref: '4',
      title: 'titulo planta 4',
      images: ['imagen 4.1', 'imagen 4.2'],
      categories: ['categoria 1, planta 4', 'categoria 2, planta 4'],
      price: 1.99,
      info: {
        height: 1.5,
        size: 'mediano de planta 4',
        description: 'descripcion planta 4'
      }
    },
  ];
}

// Plantilla generica:
// {
//   id: x,
//   ref: 'x',
//   title: 'titulo planta x',
//   images: ['imagen x.1', 'imagen x.2'],
//   categories: ['categoria 1, planta x', 'categoria 2, planta x'],
//   price: 1.99,
//   info: {
//     height: 1.5,
//     size: 'mediano de planta x',
//     description: 'descripcion planta x'
//   }
// },